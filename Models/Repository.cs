namespace Crud.Models;

public static class Repository
{
    private static List<Employee> allEmployees = new();

    public static IEnumerable<Employee> AllEmployees 
    { 
        get
        {
            return allEmployees;
        }
    }

    public static void Create(Employee emp)
    {
        allEmployees.Add(emp);
    }
}
