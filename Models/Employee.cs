namespace Crud.Models;

public class Employee
{
    public string Name {get;set;} = "";
    public int Age {get;set;}
    public decimal Salary {get;set;}
    public string Department = "";
    public char Sex {get;set;}
}
